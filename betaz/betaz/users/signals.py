from allauth.account.signals import user_signed_up


def user_signed_up_signal_handler(sender, **kwargs):
    print("User signed-up in: {}".format(kwargs))


user_signed_up.connect(user_signed_up_signal_handler)

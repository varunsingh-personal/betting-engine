# Generated by Django 3.1.13 on 2021-08-22 16:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0008_eventodds_odd_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='event_status',
            field=models.CharField(choices=[('not_started', 'Not Started'), ('on_going', 'On Going'), ('completed', 'Completed'), ('cancelled', 'Cancelled')], default='not_started', max_length=64),
        ),
    ]

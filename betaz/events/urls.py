from django.urls import path
from .views import event_list_view, event_detail_view, user_bet_view, cancel_bet_view

app_name = "events"
urlpatterns = [
    path("", view=event_list_view, name="list"),
    path("<int:pk>/", view=event_detail_view, name="detail"),
    path("<int:pk>/bet", view=user_bet_view, name="bet"),
    path("<int:pk>/bet/<int:bet_pk>/cancel", view=cancel_bet_view, name="cancel-bet"),
]

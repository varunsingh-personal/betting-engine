import logging

from django.db import transaction
from django.db.models import F

from config import celery_app
from events.models import Event, EventOdds, UserBet, UserWallet

log = logging.getLogger(__name__)


@celery_app.task()
@transaction.atomic
def perform_payout():
    log.info("Starting 'perform_payout' background job")
    for event in Event.objects.filter(event_status='completed').iterator():
        log.info("Processing event: {}".format(event))
        if EventOdds.objects.filter(event=event).exists():
            event_odds = event.eventodds
            log.debug("EventOdds")
            log.info("Event odds: {}".format(event_odds))
            for user_bet in UserBet.objects.filter(event=event, is_settled=False).iterator():
                bet_amount = user_bet.bet_amount
                if user_bet.bet_on_draw:
                    if event.match_drawn:
                        bet_odds = 0
                    else:
                        bet_odds = event_odds.draw_odds
                else:
                    if not event.winning_team:
                        log.info("Event was not a draw but no winning team given. Aborting!")
                        return
                    if user_bet.bet_on_team == event.winning_team:
                        if user_bet.bet_on_team == event_odds.team1:
                            bet_odds = event_odds.team1_odds
                        else:
                            bet_odds = event_odds.team2_odds
                    else:
                        bet_odds = 0
                won_amount = bet_amount * bet_odds
                log.info("bet_amount: {}, bet_odds: {}, won_amount: {}".format(bet_amount, bet_odds, won_amount))
                if UserWallet.objects.filter(user=user_bet.user).exists():
                    user_wallet = UserWallet.objects.get(user=user_bet.user)
                    user_wallet.money = F('money') + won_amount
                    user_wallet.save()
                    user_bet.is_settled = True
                    user_bet.save()
                    log.info("Money transfered to user's account")

    log.info("Finished 'perform_payout' background job")


@celery_app.task()
@transaction.atomic
def refund_amount():
    log.info("Starting 'refund_amount' background job")
    for event in Event.objects.filter(event_status='cancelled').iterator():
        log.info("Processing event: {}".format(event))
        for user_bet in UserBet.objects.filter(event=event, is_settled=False).iterator():
            bet_amount = user_bet.bet_amount
            if UserWallet.objects.filter(user=user_bet.user).exists():
                user_wallet = UserWallet.objects.get(user=user_bet.user)
                user_wallet.money = F('money') + bet_amount
                user_wallet.save()
                user_bet.is_settled = True
                user_bet.save()
                log.info("Money refunded to user's account")

    log.info("Finished 'refund_amount' background job")



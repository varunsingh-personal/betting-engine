from django.db import models

from betaz.users.models import User


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Team(BaseModel):
    name = models.CharField(max_length=128, unique=True, null=False, blank=False)
    logo_url = models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):
        return self.name

PLAYER_ROLE_CHOICES = (
    ('owner', 'Owner'),
    ('captain', 'Captain'),
    ('member', 'Member')
)


class Player(BaseModel):
    name = models.CharField(max_length=512, unique=True, null=False, blank=False)
    team = models.OneToOneField(Team, on_delete=models.SET_NULL, null=True, blank=True)
    role = models.CharField(max_length=64, choices=PLAYER_ROLE_CHOICES, default='member')

    def __str__(self):
        return self.name + ' -- ' + str(self.team) if self.team else ''


EVENT_STATUS_CHOICES = (('not_started', 'Not Started'),
                                             ('on_going', 'On Going'),
                                             ('completed', 'Completed'),
                                             ('cancelled', 'Cancelled'))


class Event(BaseModel):
    name = models.CharField(max_length=512, null=False, blank=False)
    date_time = models.DateTimeField(null=True, blank=True)
    sport = models.CharField(max_length=512, default="", null=True, blank=True)
    description = models.CharField(max_length=1024, default="", null=True, blank=True)
    winning_team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True, blank=True)
    match_drawn = models.BooleanField(null=True, blank=True)
    betting_closed = models.BooleanField(null=True, blank=True)
    event_status = models.CharField(max_length=64,
                                    choices=EVENT_STATUS_CHOICES,
                                    default='not_started')

    class Meta:
        ordering = ['-date_time']
    def display_event_status(self):
        return dict(EVENT_STATUS_CHOICES)[self.event_status]

    def __str__(self):
        return self.name + ' ' + self.sport if self.sport else '' + ' on ' + str(self.date_time) if self.date_time else ''


class EventTeam(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        unique_together = ('event', 'team')

    def __str__(self):
        return self.team.name if self.team else '' + ' -- ' + self.event.name


class EventPlayer(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        unique_together = ('event', 'player')

    def __str__(self):
        return (self.player.name + ' -- ' + self.player.team.name) if self.player else '' + ' ' + self.event.name


class EventOdds(BaseModel):
    event = models.OneToOneField(Event, on_delete=models.CASCADE)
    odd_type = models.CharField(max_length=64, choices=(('fixed', 'Fixed Odds'), ('pool', 'Pool Betting')), default='pool')
    team1 = models.ForeignKey(Team, related_name='team1', on_delete=models.SET_NULL, null=True, blank=True)
    team1_odds = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    team2 = models.ForeignKey(Team, related_name='team2', on_delete=models.SET_NULL, null=True, blank=True)
    team2_odds = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    draw_odds = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)

    class Meta:
        unique_together = ('event', 'team1', 'team2')

    def __str__(self):
        event_name = self.event.name if self.event else ''
        team1_name = self.team1.name if self.team1 else ''
        team2_name = self.team2.name if self.team2 else ''
        team1_odds = str(self.team1_odds) if self.team1_odds else ''
        team2_odds = str(self.team2_odds) if self.team2_odds else ''
        draw_odds = str(self.draw_odds) if self.draw_odds else ''
        return event_name + ' ' + self.odd_type + ' ' + team1_name + ': ' + team1_odds + ' ' + team2_name + ': ' + team2_odds + ' Draw Odds: ' + draw_odds


class UserBet(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    bet_on_team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True)
    bet_on_draw = models.BooleanField(null=True, blank=True)
    bet_amount = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    is_settled = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'event')

    def __str__(self):
        user_name = self.user.name if self.user else ''
        event_name = self.event.name if self.event else ''
        team_name = 'On ' + self.bet_on_team.name if self.bet_on_team else 'On Draw' if self.bet_on_draw else ''
        amount = str(self.bet_amount) if self.bet_amount else ''
        return user_name + ' -- ' + event_name + ' -- ' + team_name + ' -- AZ' + amount


class UserWallet(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    money = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.user.name + ' -- AZ' + str(self.money)


class UserRanking(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    amount_won = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)

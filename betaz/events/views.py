import logging
from decimal import Decimal
from typing import Any, Dict

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, DetailView

User = get_user_model()
from .models import Event, UserBet, Team, UserWallet, EventOdds

log = logging.getLogger(__name__)


class EventListView(ListView):
    model = Event


event_list_view = EventListView.as_view()


class EventDetailView(LoginRequiredMixin, DetailView):
    model = Event

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        event = self.get_object()
        context['bet'] = event.userbet_set.filter(user__username=self.request.user.username).first()
        teams = list()
        team1_players = list()
        team2_players = list()
        if event.eventteam_set.exists():
            for event_team in event.eventteam_set.iterator():
                teams.append(event_team.team)

        if len(teams) >= 2:
            context['team1'] = teams[0]
            if event.eventplayer_set.filter(player__team=teams[0]).exists():
                team1_players = list(event.eventplayer_set.filter(player__team=teams[0]))
            context['team1_players'] = team1_players
            context['team2'] = teams[1]
            if list(event.eventplayer_set.filter(player__team=teams[1])):
                team2_players = list(event.eventplayer_set.filter(player__team=teams[1]))
            context['team2_players'] = team2_players
        else:
            context['team1'] = None
            context['team2'] = None

        context['teams'] = teams
        event_odds = None
        try:
            event_odds = event.eventodds
        except EventOdds.DoesNotExist:
            log.warning("EventOdds do not exist for this event")
        context['event_odds'] = event_odds
        return context


event_detail_view = EventDetailView.as_view()


class UserBetView(View):

    def post(self, request, pk):
        data = request.POST
        event = Event.objects.get(pk=pk)
        auth_user = request.user
        user = User.objects.get(username=auth_user.username)
        user_wallet = UserWallet.objects.get(user=user)

        user_bet = UserBet.objects.filter(user=user, event=event).first()
        if user_bet:
            bet_amount = user_bet.bet_amount
            user_wallet.money = F('money') + bet_amount
            user_wallet.save()
            user_bet.delete()
        user_bet = UserBet()
        user_bet.user = user
        user_bet.event = Event.objects.get(pk=pk)
        if user_bet.is_settled:
            messages.info(request, 'Bet already settled!')
        else:
            if data["team"] != "Draw":
                user_bet.bet_on_team = Team.objects.get(pk=data['team'])
            else:
                user_bet.bet_on_team = None
                user_bet.bet_on_draw = True
            user_wallet.money = F('money') - Decimal(data['amount'])
            user_wallet.save()
            user_bet.bet_amount = data['amount']
            user_bet.save()
            messages.info(request, 'Bet Added! All the best.')
        return HttpResponseRedirect(reverse('events:detail', args=(pk,)))


user_bet_view = UserBetView.as_view()


class CancelBetView(View):

    def get(self, request, pk, bet_pk):
        user_bet = UserBet.objects.get(pk=bet_pk)
        auth_user = request.user
        user = User.objects.get(username=auth_user.username)
        user_wallet = UserWallet.objects.get(user=user)
        if user_bet.user.username == auth_user.username:
            bet_amount = user_bet.bet_amount
            user_wallet.money = F('money') + bet_amount
            user_wallet.save()
            user_bet.delete()
            messages.info(request, 'Bet Cancelled!')
        else:
            messages.warning(request, 'Cannot cancel this bet!')

        return HttpResponseRedirect(reverse('events:detail', args=(pk,)))


cancel_bet_view = CancelBetView.as_view()

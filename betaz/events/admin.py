from django.contrib import admin

# Register your models here.
from .models import Team, Player, Event, EventPlayer, EventOdds, UserWallet, UserRanking, UserBet, EventTeam


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    pass


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    pass


class EventTeamTabularInline(admin.TabularInline):
    model = EventTeam
    max_num = 2


class EventPlayerTabularInline(admin.TabularInline):
    model = EventPlayer
    max_num = 2


class EventOddsTabularInline(admin.TabularInline):
    model = EventOdds


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    inlines = [EventTeamTabularInline, EventOddsTabularInline, EventPlayerTabularInline]


@admin.register(UserWallet)
class UserWalletAdmin(admin.ModelAdmin):
    pass


@admin.register(UserBet)
class UserBetAdmin(admin.ModelAdmin):
    pass
